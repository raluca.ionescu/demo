package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.DeviceDTO;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import java.util.List;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")

public class DeviceServiceIntegrationTest extends Ds2020TestConfig {

    @Autowired
    DeviceService deviceService;

    @Test
    public void testGetCorrect() {
        List<DeviceDTO> personDTOList = deviceService.findDevices();
        assertEquals("Test Insert Device", 1, personDTOList.size());
    }

}
