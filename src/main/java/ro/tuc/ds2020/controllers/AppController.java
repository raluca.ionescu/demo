package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.Record;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.RecordService;
import ro.tuc.ds2020.services.RegistrationService;
import ro.tuc.ds2020.services.SensorService;

import javax.transaction.Transactional;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin

public class AppController {


    @Autowired
    private RegistrationService serv;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private SensorService sensorService;

    @Autowired
    private RecordService recordService;

    @PostMapping("/register")
    public Client regiterUser(@RequestBody Client user) throws Exception
    {
        String tempEmailId = user.getEmail();
        String passWord = user.getPassword();

        if(tempEmailId != null) {

            Client userObj = serv.fetchUserByEmailId(tempEmailId);

            if(userObj!=null) {
                throw new Exception("user with " + tempEmailId +  " already exist!" );
            }

            if(tempEmailId.equals("admin@admin.ro") && passWord.equals("Admin1234!")) {
                user.setRole("admin");
            }
            else {
                user.setRole("user");
            }

        }
        Client userObj = null;

        userObj = serv.saveUser(user);

        return userObj;
    }


    @Transactional
    @CrossOrigin
    @PostMapping("/login")
    public Client loginUser(@RequestBody ClientLoginDTO user) throws Exception{
        String tempEmailId = user.getEmail();
        String tempPassword = user.getPassword();
        Client userObj = null;
        if(tempEmailId != null && tempPassword!=null) {
            userObj = serv.fetchUserByEmailAndPassword(tempEmailId, tempPassword);
        }

        if(userObj == null) {
            throw new Exception("This user does not exist!");
        }

        return userObj;
    }

    @PostMapping("/device")
    public ResponseEntity<Device> addDevice(@RequestBody Device device) {
        return new ResponseEntity<>(this.deviceService.saveDevice(device),HttpStatus.CREATED);
    }

    @PostMapping("/sensor")
    public Sensor addSensor(@RequestBody Sensor sensor) {
        return this.sensorService.saveSensor(sensor);
    }

    @PostMapping("/records")
    public Record addRecord(@RequestBody Record record) {
        return this.recordService.saveRecord(record);
    }

    @GetMapping("/client")
    public ResponseEntity<List<ClientDTO>> getPersons() {
        List<ClientDTO> dtos = serv.findClients();
        for (ClientDTO dto : dtos) {
            Link clientLink = linkTo(((AppController) methodOn(AppController.class))
                    .getClient(dto.getId())).withRel("personDTO");
            Link devicesLink = linkTo(((AppController) methodOn(AppController.class))
                    .getClientDevices(dto.getId())).withRel("devices");
            dto.add(clientLink);
            dto.add(devicesLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/devices/{id}")
    public List<DeviceDTO> getClientDevices(@PathVariable("id") Integer clientId){

        return serv.findClientDevices(clientId);
    }

    @GetMapping("/device")
    public ResponseEntity<List<DeviceDTO>> getDevices() {
        List<DeviceDTO> dtos = deviceService.findDevices();
        for (DeviceDTO dto : dtos) {
            Link deviceLink = linkTo(((AppController) methodOn(AppController.class))
                    .getDevice(dto.getId())).withRel("deviceDTO");
            dto.add(deviceLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/sensor")
    public ResponseEntity<List<SensorDTO>> getSensors() {
        List<SensorDTO> dtos = sensorService.findSensors();
        for (SensorDTO dto : dtos) {
            Link sensorLink = linkTo(((AppController) methodOn(AppController.class))
                    .getSensor(dto.getId())).withRel("SensorDTO");
            dto.add(sensorLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/sensorswithrecords")
    public List<SensorRecordDTO> getSensorsWithRecords(){
        List<SensorRecordDTO> dtos = sensorService.findSensorsWithRecords();
        return dtos;
    }
    @GetMapping("/records")
    public List<RecordDTO> GetRecords(){
        return this.recordService.findAll();
    }

    @GetMapping(value = "/client/{id}")
    public ClientDTO getClient(@PathVariable("id") int personId) {
        ClientDTO dto = serv.findClientById(personId);
        return dto;
    }

    @GetMapping(value = "/device/{id}")
    public DeviceDTO getDevice(@PathVariable("id") int deviceId) {
        DeviceDTO dto = deviceService.findDeviceById(deviceId);
        return dto;
    }

    @GetMapping(value = "/sensor/{id}")
    public SensorDTO getSensor(@PathVariable("id") int i) {
        SensorDTO dto = sensorService.findSensorsById(i);
        return dto;
    }

    @GetMapping("/records/{id}")
    public RecordDTO getRecordById(@PathVariable("id") int id) {
        return this.recordService.findById(id);
    }


    @DeleteMapping(value= "/client/{id}")
    public void deleteClient(@PathVariable("id") int clientId) {
        serv.deleteClient(clientId);
    }

    @DeleteMapping(value = "/device/{id}")
    public void deleteDevice(@RequestParam(value="sensor",required = false) Integer sensorId,@PathVariable("id") int deviceId) {
        if(sensorId!=null) {

            deviceService.deleteSensor(deviceId,sensorId);
        }
        else{
            deviceService.deleteDevice(deviceId);
        }
    }

    @DeleteMapping(value = "/sensor/{id}")
    public void deleteSensor(@PathVariable("id") int id) {
        sensorService.deleteSensor(id);
    }

    @DeleteMapping("/records/{id}")
    public void deleteRecordById(@PathVariable("id") int id) {
        this.recordService.deleteRecord(id);
    }


    @PutMapping("/client")
    public Client updateClient(@RequestParam(value="id",required = false)Integer deviceId, @RequestBody Client client) {
        if(deviceId!=null) {

            Device device = this.deviceService.findDevicesById(deviceId);
            System.out.println(device.getDescription());
            return serv.addDeviceToClient(device, client);
        }
        return serv.updateClient(client);
    }

    @PutMapping("/device")
    public Device updateDevice(@RequestParam(value="id",required = false) Integer sensorId, @RequestBody Device device) {
        if(sensorId!=null) {
            Sensor sensor = this.sensorService.findSensorById(sensorId);
            return deviceService.addSensor(device,sensor);
        }
        return deviceService.updateDevice(device);
    }

    @PutMapping("/sensor")
    public Sensor updateSensor(@RequestBody Sensor sensor) {
        return sensorService.updateSensor(sensor);
    }




}
