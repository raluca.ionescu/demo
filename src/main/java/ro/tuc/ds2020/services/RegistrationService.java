package ro.tuc.ds2020.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Client;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Record;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.RecordRepository;
import ro.tuc.ds2020.repositories.RegistrationRepository;
import ro.tuc.ds2020.repositories.SensorRepository;



@Service
public class RegistrationService {

    @Autowired
    private RegistrationRepository repo;

    public Client saveUser(Client user) {
        return repo.save(user);
    }

    public Client fetchUserByEmailId(String tempEmailId) {
        return repo.findByEmail(tempEmailId);
    }

    public Client fetchUserByEmailAndPassword(String email, String password) {
        return repo.findByEmailAndPassword(email, password);
    }

    public Optional<Client> findById(int personId) {
        return repo.findById(personId);
    }

    public List<ClientDTO> findClients() {
        List<Client> personList = repo.findAll();
        return personList.stream()
                .map(ClientBuilder::toClientDTO)
                .collect(Collectors.toList());
    }

    public ClientDTO findClientById(int id) {
        Optional<Client> prosumerOptional = repo.findById(id);
        if (!prosumerOptional.isPresent()) {
        }
        return ClientBuilder.toClientDTO(prosumerOptional.get());
    }

    public void deleteClient(int clientId) {
        // TODO Auto-generated method stub
        repo.deleteById(clientId);
    }

    public Client updateClient(Client client) {
        Client existingClient = repo.findById(client.getId()).orElse(null);
        existingClient.setFirstName(client.getFirstName());
        existingClient.setLastName(client.getLastName());
        existingClient.setAddress(client.getAddress());
        existingClient.setBirthdate(client.getBirthdate());
        return repo.save(existingClient);
    }

    public Client addDeviceToClient(Device device, Client client) {
        Client existingClient = repo.findById(client.getId()).orElse(null);

        if(!device.getIsAssociated()) {
            device.setAddress(existingClient.getAddress());
            device.setIsAssociated(true);
            device.setClient(existingClient);
            existingClient.setDevices(device);
        }

        return repo.save(existingClient);
    }

    public List<DeviceDTO> findClientDevices(Integer clientId) {
        Client existingClient = repo.findById(clientId).orElse(null);
        Set<Device> devices = (Set<Device>) existingClient.getDevices();
        return devices.stream().map(DeviceBuilder::toDeviceDTO).collect(Collectors.toList());
    }




}