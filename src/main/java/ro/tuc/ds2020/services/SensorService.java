package ro.tuc.ds2020.services;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.SensorBuilder;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.SensorRecordBuilder;
import ro.tuc.ds2020.dtos.SensorRecordDTO;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.SensorRepository;

@Service
public class SensorService {

    @Autowired
    private SensorRepository repo;

    public List<SensorDTO> findSensors() {

        List<Sensor> sensorList = repo.findAll();

        return sensorList.stream()
                .map(SensorBuilder::toSensorDTO)
                .collect(Collectors.toList());
    }


    public SensorDTO findSensorsById(int sensorId) {
        Optional<Sensor> prosumerOptional = repo.findById(sensorId);
        if (!prosumerOptional.isPresent()) {
        }
        return SensorBuilder.toSensorDTO(prosumerOptional.get());
    }


    public void deleteSensor(int id) {
        repo.deleteById(id);
    }


    public Sensor updateSensor(Sensor sensor) {

        Sensor existingSensor = repo.findById(sensor.getId()).orElse(null);
        existingSensor.setDescription(sensor.getDescription());
        existingSensor.setMax_value(sensor.getMax_value());
        return repo.save(existingSensor);

    }

    public Sensor saveSensor(Sensor device) {
        return this.repo.save(device);
    }


    public Sensor findSensorById(Integer sensorId) {
        return this.repo.findById(sensorId).orElse(null);
    }


    public List<SensorRecordDTO> findSensorsWithRecords() {
        List<Sensor> sensorList = repo.findAll();

        return sensorList.stream()
                .map(SensorRecordBuilder::toSensorRecordDTO)
                .collect(Collectors.toList());
    }






}