package ro.tuc.ds2020.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DeviceBuilder;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.DeviceRepository;
import ro.tuc.ds2020.repositories.SensorRepository;


@Service
public class DeviceService {

    @Autowired
    private DeviceRepository repo;

    @Autowired
    private SensorRepository sensorRepo;
    public List<DeviceDTO> findDevices() {

        List<Device> deviceList = repo.findAll();
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDTO findDeviceById(int id) {
        Optional<Device> prosumerOptional = repo.findById(id);
        if (!prosumerOptional.isPresent()) {
        }
        return DeviceBuilder.toDeviceDTO(prosumerOptional.get());
    }

    public void deleteDevice(int deviceId) {
        repo.deleteById(deviceId);
    }


    public Device updateDevice(Device device) {

        Device existingDevice = repo.findById(device.getId()).orElse(null);
        existingDevice.setDescription(device.getDescription());
        existingDevice.setMax_energy(device.getMax_energy());
        existingDevice.setAverage_energy(device.getAverage_energy());
        return repo.save(existingDevice);

    }

    public Device saveDevice(Device device) {
        return this.repo.save(device);
    }


    public Device addSensor(Device device, Sensor sensor) {
        Device existingDevice = repo.findById(device.getId()).orElse(null);
        if(!sensor.isAssociated()) {
            sensor.setAssociated(true);
            existingDevice.setSensor(sensor);
        }
        return repo.save(existingDevice);
    }


    public Device findDevicesById(Integer deviceId) {
        return repo.findById(deviceId).orElse(null);
    }


    public void deleteSensor(int deviceId, int sensorId) {
        Device existingDevice = repo.findById(deviceId).orElse(null);
        Sensor sensor = sensorRepo.findById(sensorId).orElse(null);
        if(sensor.getIsAssociated()) {
            sensor.setIsAssociated(false);
            existingDevice.setSensor(null);
            existingDevice.setIsAssociated(false);
        }
        sensorRepo.save(sensor);
        repo.save(existingDevice);

    }

}