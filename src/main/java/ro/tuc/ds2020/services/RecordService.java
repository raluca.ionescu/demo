package ro.tuc.ds2020.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.tuc.ds2020.dtos.DeviceBuilder;
import ro.tuc.ds2020.dtos.RecordBuilder;
import ro.tuc.ds2020.dtos.RecordDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Record;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.repositories.RecordRepository;
import ro.tuc.ds2020.repositories.SensorRepository;

@Service
public class RecordService {

    @Autowired
    private RecordRepository repo;

    @Autowired
    private SensorRepository sensorRepo;

    public Record saveRecord(Record record) {

        return repo.save(record);
    }

    public List<RecordDTO> findAll() {

        List<Record> records = repo.findAll();
        return records.stream().map(RecordBuilder::toRecordDTO)
                .collect(Collectors.toList());
    }

    public RecordDTO findById(int id) {
        Record r = repo.findById(id).orElse(null);

        return RecordBuilder.toRecordDTO(r);

    }

    public void deleteRecord(int id) {
        repo.deleteById(id);
    }
}
