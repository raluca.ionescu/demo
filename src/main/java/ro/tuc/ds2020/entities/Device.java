package ro.tuc.ds2020.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "device")
public class Device {

    @Id
    @GeneratedValue
    private int id;

    private String description;

    private long max_energy;

    private float average_energy;

    private String address;

    @ManyToOne
    @JoinColumn(name="client")
    private Client client;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="sensor", referencedColumnName = "id")
    private Sensor sensor;

    @Column(columnDefinition="BOOLEAN DEFAULT false")
    private Boolean isAssociated = false;


    public Device() {}
    public Device(String d, long m, float a, String address) {
        this.description = d;
        this.average_energy = a;
        this.max_energy = m;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getMax_energy() {
        return max_energy;
    }

    public void setMax_energy(long max_energy) {
        this.max_energy = max_energy;
    }

    public float getAverage_energy() {
        return average_energy;
    }

    public void setAverage_energy(float average_energy) {
        this.average_energy = average_energy;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor2) {
        this.sensor = sensor2;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public Boolean getIsAssociated() {
        return isAssociated;
    }
    public void setIsAssociated(Boolean isAssociated) {
        this.isAssociated = isAssociated;
    }






}