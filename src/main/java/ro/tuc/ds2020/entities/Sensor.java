package ro.tuc.ds2020.entities;


import java.util.List;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "sensor")
public class Sensor {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    private String description;

    private long max_value;

    @Column(columnDefinition="BOOLEAN DEFAULT false")
    private Boolean isAssociated = false;

    @JsonBackReference
    @OneToOne(mappedBy="sensor")
    private Device device;


    @OneToMany(mappedBy="sensor", cascade = CascadeType.MERGE, fetch= FetchType.EAGER)
    private List<Record> records;

    public Boolean getIsAssociated() {
        return isAssociated;
    }

    public void setIsAssociated(Boolean isAssociated) {
        this.isAssociated = isAssociated;
    }

    public Sensor() {}

    public Sensor(String d, long m) {
        description = d;
        max_value = m;
        this.isAssociated = false;
        this.records = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getMax_value() {
        return max_value;
    }

    public void setMax_value(long max_value) {
        this.max_value = max_value;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;

    }

    public boolean isAssociated() {
        return isAssociated;
    }

    public void setAssociated(boolean isAssociated) {
        this.isAssociated = isAssociated;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public void addRecord(Record record) {
        this.records.add(record);
    }



}