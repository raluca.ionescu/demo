package ro.tuc.ds2020.dtos;


import java.sql.Timestamp;
import java.time.LocalDateTime;

public class RecordDTO {

    private int id;

    private SensorDTO sensor;

    private LocalDateTime timestamp;

    private double energy;


    public RecordDTO(int id, SensorDTO sensor, LocalDateTime timestamp, double energy) {
        super();
        this.id = id;
        this.sensor = sensor;
        this.timestamp = timestamp;
        this.energy = energy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SensorDTO getSensor() {
        return sensor;
    }

    public void setSensor(SensorDTO sensor) {
        this.sensor = sensor;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

}