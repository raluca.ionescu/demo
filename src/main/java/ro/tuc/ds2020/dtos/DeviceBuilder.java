package ro.tuc.ds2020.dtos;


import ro.tuc.ds2020.entities.Device;

public class DeviceBuilder {

    private DeviceBuilder() {}

    public static DeviceDTO toDeviceDTO(Device device) {

        SensorDTO sensor = device.getSensor()!=null ? SensorBuilder.toSensorDTO(device.getSensor()) : null;
        ClientDTO client = device.getClient()!=null ?  ClientBuilder.toClientDTO(device.getClient()) : null;

        return new DeviceDTO(device.getId(), device.getDescription(),
                device.getMax_energy(), device.getAverage_energy(), sensor, device.getIsAssociated(), client);
    }

}
