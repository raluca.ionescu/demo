package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

public class DeviceDTO extends RepresentationModel<DeviceDTO>{

    private int id;
    private String description;
    private long max_energy;
    private float average_energy;
    private SensorDTO sensor;
    private boolean associated;
    private ClientDTO client;

    public DeviceDTO(int id, String d, long m, float a, SensorDTO sensor2, boolean associated, ClientDTO client ) {
        this.id = id;
        this.description = d;
        this.average_energy = a;
        this.max_energy = m;
        this.sensor = sensor2;
        this.associated = associated;
        this.client = client;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getMax_energy() {
        return max_energy;
    }

    public void setMax_energy(long max_energy) {
        this.max_energy = max_energy;
    }

    public float getAverage_energy() {
        return average_energy;
    }

    public void setAverage_energy(float average_energy) {
        this.average_energy = average_energy;
    }

    public SensorDTO getSensor() {
        return sensor;
    }

    public void setSensor(SensorDTO sensor) {
        this.sensor = sensor;
    }


    public boolean isAssociated() {
        return associated;
    }


    public void setAssociated(boolean associated) {
        this.associated = associated;
    }


    public ClientDTO getClient() {
        return client;
    }


    public void setClient(ClientDTO client) {
        this.client = client;
    }
}
