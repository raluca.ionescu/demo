package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Record;
import ro.tuc.ds2020.entities.Sensor;

public class SensorBuilder {

    private SensorBuilder() {}

    public static SensorDTO toSensorDTO(Sensor sensor) {

        double energy = 0;

        for(Record r: sensor.getRecords()) {
            energy+= r.getEnergy();
        }

        return  new SensorDTO(sensor.getId(), sensor.getDescription(), sensor.getMax_value(), sensor.getIsAssociated(), energy);

    }
}

