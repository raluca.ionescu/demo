package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Record;

public class RecordBuilder {

    public RecordBuilder() {}

    public static RecordDTO toRecordDTO(Record record) {
        SensorDTO sensor = SensorBuilder.toSensorDTO(record.getSensor());
        return new RecordDTO(record.getId(), sensor, record.getTimestamp(), record.getEnergy());

    }
}
