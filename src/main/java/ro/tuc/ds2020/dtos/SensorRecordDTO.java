package ro.tuc.ds2020.dtos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

public class SensorRecordDTO extends RepresentationModel<SensorRecordDTO>{

    private int id;

    private String description;

    private long max_value;


    private List<RecordDTO> records = new ArrayList<>();


    public SensorRecordDTO(int id, String description, long max_value,List<RecordDTO> records) {
        this.id = id;
        this.description = description;
        this.max_value = max_value;

        this.records = records;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getMax_value() {
        return max_value;
    }

    public void setMax_value(long max_value) {
        this.max_value = max_value;
    }




    public List<RecordDTO> getRecords() {
        return records;
    }

    public void setRecords(List<RecordDTO> records) {
        this.records = records;
    }



}
