package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.Record;
import java.util.List;
import java.util.stream.Collectors;

public class SensorRecordBuilder {
    private SensorRecordBuilder() {}

    public static SensorRecordDTO toSensorRecordDTO(Sensor sensor) {

        List<RecordDTO> records = sensor.getRecords().stream().map(RecordBuilder::toRecordDTO).collect(Collectors.toList());

        return  new SensorRecordDTO(sensor.getId(), sensor.getDescription(),
                sensor.getMax_value(), records);

    }
}
