package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Client;

public class ClientBuilder {
    private ClientBuilder() {

    }

    public static ClientDTO toClientDTO(Client client) {
        return new ClientDTO(client.getId(), client.getFirstName(), client.getLastName(), client.getAddress()
                , client.getBirthdate(), client.getEmail(), client.getRole());
    }

}
