package ro.tuc.ds2020.dtos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

public class SensorDTO extends RepresentationModel<SensorDTO>{

    private int id;

    private String description;

    private long max_value;

    private Boolean isAssociated;

    private double totalEnergy = 0;


    public SensorDTO(int id, String description, long max_value, boolean a, double e) {
        this.id = id;
        this.description = description;
        this.max_value = max_value;
        this.isAssociated = a;
        this.totalEnergy = e;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getMax_value() {
        return max_value;
    }

    public void setMax_value(long max_value) {
        this.max_value = max_value;
    }

    public boolean isAssociated() {
        return isAssociated;
    }

    public void setAssociated(boolean isAssociated) {
        this.isAssociated = isAssociated;
    }

    public double getTotalEnergy() {
        return this.totalEnergy;
    }

    public void setEnergy(double e) {
        this.totalEnergy = e;
    }


    public void setTotalEnergy(double totalEnergy) {
        this.totalEnergy = totalEnergy;
    }


}