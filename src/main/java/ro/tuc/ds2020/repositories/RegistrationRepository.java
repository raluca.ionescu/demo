package ro.tuc.ds2020.repositories;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Client;


public interface RegistrationRepository extends JpaRepository<Client, Integer> {


    Client findByEmail(String tempEmailId);

    Client findByEmailAndPassword(String email, String password);

    Client findById(Optional<Integer> id);


}
